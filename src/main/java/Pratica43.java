
import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Elipse;
import utfpr.ct.dainf.if62c.pratica.Quadrado;
import utfpr.ct.dainf.if62c.pratica.Retangulo;
import utfpr.ct.dainf.if62c.pratica.TrianguloEquilatero;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica43 {
    public static void main(String[] args) {
        Elipse a = new Elipse(5,2);
        Circulo b = new Circulo(5);
        Quadrado c = new Quadrado(5);
        Retangulo d = new Retangulo(5,2);
        TrianguloEquilatero e = new TrianguloEquilatero(5);
        
        
        System.out.println(c.getArea());
        System.out.println(c.getPerimetro());
        
        System.out.println(d.getArea());
        System.out.println(d.getPerimetro());
        
        System.out.println(e.getArea());
        System.out.println(e.getPerimetro());
        
        
    }
}
