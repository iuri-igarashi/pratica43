/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author a1100629
 */
public class Retangulo implements FiguraComLados{
    
    public double ladoMaior, ladoMenor;

    public Retangulo(double ladoMaior, double ladoMenor) {
        this.ladoMaior = ladoMaior;
        this.ladoMenor = ladoMenor;
    }
    
    @Override
    public double getLadoMenor() {
        return ladoMenor;
    }

    @Override
    public double getLadoMaior() {
        return ladoMaior;
    }

    @Override
    public double getPerimetro() {
        return (ladoMaior*2) + (ladoMenor*2);
    }

    @Override
    public double getArea() {
        return ladoMaior * ladoMenor;
    }
    
}
