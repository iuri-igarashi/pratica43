/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author a1100629
 */
public class Quadrado extends Retangulo{

    public Quadrado(double lado) {
        super(lado, lado);
    }
    
    @Override
    public double getLadoMenor() {
        return ladoMaior;
    }

    @Override
    public double getLadoMaior() {
        return ladoMaior;
    }

    @Override
    public double getPerimetro() {
        return ladoMaior * 4;
    }
    
}
