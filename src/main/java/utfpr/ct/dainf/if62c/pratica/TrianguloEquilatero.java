/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author a1100629
 */
public class TrianguloEquilatero extends Retangulo{
    double base, altura;
    
    public TrianguloEquilatero(double base) {
        super(base, base);
        this.base = ladoMenor;
        this.altura = Math.sqrt(Math.pow(base,2) - Math.pow(base/2, 2));
    }
    
    @Override
    public double getLadoMenor() {
        return base;
    }

    @Override
    public double getLadoMaior() {
        return altura;
    }

    @Override
    public double getPerimetro() {
        return base * 3;
    }

    @Override
    public double getArea() {
        return (base * altura)/2;
    }
    
}
