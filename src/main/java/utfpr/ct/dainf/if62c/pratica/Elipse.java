/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author a1100629
 */
public class Elipse implements FiguraComEixos{
    
    public double eixoMenor, eixoMaior;
    
    public Elipse(double eixoMenor, double eixoMaior) {
        this.eixoMenor = eixoMenor;
        this.eixoMaior = eixoMaior;
    }

    public Elipse() {
    }
    
    
    @Override
    public double getEixoMenor() {
        return eixoMenor;
    }

    @Override
    public double getEixoMaior() {
        return eixoMaior;
    }
    
    @Override
    public double getPerimetro() {
        double r = getEixoMaior() / 2;
        double s = getEixoMenor() / 2;
        return Math.PI * ( ((3*r)+s) - Math.sqrt((3*r)+s) + (r+(3*s)));
    }

    @Override
    public double getArea() {
        double r = getEixoMaior() / 2;
        double s = getEixoMenor() / 2;
        return Math.PI * r * s;
    }
    
}
